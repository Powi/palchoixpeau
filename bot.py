#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

#Command line parameter:
#1: Discord TOKEN

#############################################
#############################################
###                                       ###
###  Ce bot est distribué sous licence    ###
###    Affero GPL, version 3 ou plus      ###
###             ©Powi DENIS               ###
###                                       ###
#############################################
#############################################

import discord
import sys
import json
import traceback
import io
import re
import asyncio
import random
import dateutil.parser

# Init Discord client
if len(sys.argv) < 1:
	print("Usage: "+sys.argv[0]+" <DISCORD_TOKEN>")
	exit(0)

discord_token = sys.argv[1]
client = discord.Client()

def findRole(server,name):
	for r in server.roles:
		if r.name == name:
			return r
	return

def findChannel(server,name):
	for c in server.channels:
		if c.name == name:
			return c
	return

def citation(source):
	if source == "kaamelott":
		base = []
		base.append({"texte":"Ah, Biographie ! (Pet)… Biographie…","source":"Le roi burgonde","passage":"Livre I, 45 : L’interprète"})
		base.append({"texte":"Arthour !… Couhillère !","source":"Le roi burgonde","passage":"Livre II, 44 : Le dialogue de paix"})
		base.append({"texte":"La fleur en bouquet fane, et jamais ne renaît !","source":"Le roi burgonde","passage":"Livre II, 44 : Le dialogue de paix"})
		base.append({"texte":"Jouer ! Guerre ! Salsifis !","source":"Le roi burgonde","passage":"Livre IV, 66 : Le jeu de la guerre"})
		base.append({"texte":"Arthour, j’apprécie les fruits au sirop !","source":"Le roi burgonde","passage":"Livre IV, 66 : Le jeu de la guerre"})
		
		base.append({"texte":"Y a pas à dire, dès qu'il y a du dessert, le repas est tout de suite plus chaleureux !","source":"Arthur","passage":"Livre I, La tarte aux myrtilles"})
		base.append({"texte":"Mais Bohort, j'vais vous faire mettre au cachot [...]. Non mais j'vous écoute, j'vous écoute seulement j'vous préviens, j'vous l'dis, j'vais vous faire descendre en cabane avec un pichet de flotte et un bout de pain sec. J'suis désolé, j'suis démuni, j'vois pas d'autre solution. Et puis j'pense que ça vous donnera un peu l'occasion de... de réfléchir un peu à tout ça à tête reposée, de prendre un peu d'recul sur les choses parce que Bohort, on n'réveille pas son roi en pleine nuit pour des conneries, encore moins deux fois d'suite...","source":"Arthur","passage":"Livre I, Haunted"})
		base.append({"texte":"Nouvelle technique : on passe pour des cons, les autres se marrent, et on frappe. C’est nouveau. […] Ah non, ça c’est que nous. Parce qu’il faut être capable de passer pour des cons en un temps record. Ah non, là-dessus on a une avance considérable.","source":"Arthur","passage":"Livre I, La Dent de requin"})
	return random.choice(base)

@client.event
async def on_ready():
	print("* Bot "+client.user.name+" logged successfully")
		
@client.event
async def on_message(message):
	try:
		if not message.server:
			return
		
		if message.author.bot:
			return
		if message.content.find("!") != 0 and message.content.find("<@!"+client.user.id+"> ") != 0:
			return
		
		
		msgContent = message.content[len("!"):].strip()
		msgKeywords = msgContent.split(" ")
		if len(msgKeywords) == 0:
			return
		
		cmd = msgKeywords[0].strip()
		
		if cmd == "test":
			return
		elif cmd == "kaamelott":
			quote = citation("kaamelott")
			text = "`" + quote["texte"] + "`\n**" + quote["source"] + "**, *" + quote["passage"] + "*"
			await client.send_message(message.channel,text)
			return
			

	except:
		await client.send_message(message.channel, "Owh. J’ai euh… Un raté. Euh… Désolé·e.")
		print(traceback.format_exc())

@client.event
async def on_member_join(member):
	try:
		role = findRole(member.server,"Membre")
		await client.add_roles(member,role)
		
		welcome_message = "Salutations " + member.mention + " et la bienvenue dans " + member.server.name + " o/ Penses à lire la description du serveur dans " + findChannel(member.server,"presentation_du_serveur").mention + ". Tu peux si tu le souhaites t'attribuer toi-même des rôles existants et/ou en créer."
		channel = findChannel(member.server,"general")
		await client.send_message(channel,welcome_message)
		
	except:
		print(traceback.format_exc())
		

client.run(discord_token)
